#include "theatermodeapp.h"

#include <Ecore.h>
#include <Eina.h>
#include <efl_util.h>
#include <efl_extension.h>
#include <app_manager.h>
#include <glib.h>
#include <device/display.h>
#include <device/power.h>
#include <device/callback.h>
#include <system_info.h>
#include <unistd.h>

#include "simple_log.h"

static Eina_Bool power_off_button_pressed = EINA_FALSE;
static Ecore_Timer *screen_off_timer = NULL;

static bool isDnD() {
	dlog_print(DLOG_INFO, LOG_TAG, "checking DnD...");
	bool doNotDisturb = false;

	FILE *fp;
	char line[1035];

	/* Open the command for reading. */
	fp = popen("/usr/bin/vconftool get db/setting/blockmode_wearable", "r");
	if (fp == NULL) {
		dlog_print(DLOG_ERROR, LOG_TAG,
				"failed to run vconftool get db/setting/blockmode_wearable");
	}
	char* output = "";
	while (fgets(line, sizeof(line) - 1, fp) != NULL) {
		output = g_strconcat(output, line, NULL);
	}
	/* close */
	pclose(fp);

	dlog_print(DLOG_INFO, LOG_TAG,
			"vconftool get db/setting/blockmode_wearable result : %s", output);

	if (strstr(output, "value = 1") != NULL) {
		doNotDisturb = true;
	}
	dlog_print(DLOG_INFO, LOG_TAG, "doNotDisturb : %d", doNotDisturb);

	return doNotDisturb;
}

static Eina_Bool hide_window(void *data) {
	Evas_Object* win = data;
	evas_object_hide(win);
	evas_object_del(win);
	return ECORE_CALLBACK_CANCEL;
}

bool is_app_running(const char *packageId) {
	bool running = false;
	if (app_manager_is_running(packageId, &running) == APP_MANAGER_ERROR_NONE) {
		LOG(DLOG_INFO, "Running status of %s : , %d", packageId, running);
	} else {
		LOG(DLOG_ERROR, PACKAGE, "Cannot retrieve running status of app %s!",
				packageId);
	}
	return running;
}
static Evas_Object* show_disabled_popup() {
	Evas_Object *win = create_top_most_window("Theater Mode Disabled Popup"),
			*bg = create_window_background(win), *box, *label;

	box = elm_box_add(win);
	evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_object_content_set(bg, box);
	evas_object_show(box);

	label = elm_label_add(win);
	elm_object_text_set(label, "<align=center>Theater Mode is <br/>"
			"disabled. Don't <br/>forget to <br/>disable "
			"\"Do Not <br/>Disturb\" mode.</align>");
	elm_label_line_wrap_set(label, ELM_WRAP_WORD);
	evas_object_size_hint_align_set(label, 0.5, 0.5);
	evas_object_size_hint_weight_set(label, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_show(label);
	elm_box_pack_end(box, label);

	evas_object_show(win);

	return win;
}

static Evas_Object* show_enable_dnd_popup() {
	Evas_Object *win = create_top_most_window("Enable DnD Popup"), *bg =
			create_window_background(win), *box, *label;

	box = elm_box_add(win);
	evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_object_content_set(bg, box);
	evas_object_show(box);

	label = elm_label_add(win);
	elm_object_text_set(label,
			"<aling=center>Please enable <br/> \"Do Not Disturb\" <br/> mode first.</align>");
	elm_label_line_wrap_set(label, ELM_WRAP_WORD);
	evas_object_size_hint_align_set(label, 0.5, 0.5);
	evas_object_size_hint_weight_set(label, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_show(label);
	elm_box_pack_end(box, label);

	evas_object_show(win);

	return win;
}

Evas_Object *create_top_most_window(const char* title) {
	Evas_Object *win;
	win = elm_win_add(NULL, PACKAGE, ELM_WIN_NOTIFICATION);
	if (!win) {
		LOG(DLOG_ERROR, "Cannot create top most window!");
		return NULL;
	}
	int res;
	res = efl_util_set_notification_window_level(win, EFL_UTIL_NOTIFICATION_LEVEL_3);
	check_result(res, "efl_util_set_notification_window_level");
	elm_win_title_set(win, title);
	return win;
}

Evas_Object *create_window_background(Evas_Object* win) {
	Evas_Object *bg;
	bg = elm_bg_add(win);
	if (!bg) {
		evas_object_del(win);
		LOG(DLOG_ERROR, "Cannot create background for window!");
		return NULL;
	}
	evas_object_size_hint_weight_set(bg, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_win_resize_object_add(win, bg);

	evas_object_show(bg);

	return bg;
}

static void turn_screen_on() {
	int result;
	result = device_display_change_state(DISPLAY_STATE_NORMAL);
	check_result(result, "device_display_change_state");
}

static void turn_screen_off() {
	int result;
	result = device_display_change_state(DISPLAY_STATE_SCREEN_OFF);
	check_result(result, "device_display_change_state");
}
static Eina_Bool screen_off_timer_cb(void *data) {
	screen_off_timer = NULL;
	turn_screen_off();
	return ECORE_CALLBACK_CANCEL;
}

static void start_screen_off_timer() {
	if (screen_off_timer != NULL) {
		ecore_timer_reset(screen_off_timer);
	} else {
		screen_off_timer = ecore_timer_add(3, screen_off_timer_cb, NULL);
	}
}
static void delete_start_off_timer() {
	if (screen_off_timer != NULL) {
		ecore_timer_del(screen_off_timer);
	}
}

static void screen_state_cb(device_callback_e type, void *value,
		void *user_data) {
	display_state_e *state = value;
	check_result(state, "display_state_e");
	if (state != DISPLAY_STATE_SCREEN_OFF) {
		start_screen_off_timer();
	} else {
		power_off_button_pressed = EINA_FALSE;
	}
}

void unregister_callbacks() {
	int result;
	result = device_remove_callback(DEVICE_CALLBACK_DISPLAY_STATE,
			screen_state_cb);
	check_result(result, "device_remove_callback");
	delete_start_off_timer();
}
Eina_Bool _rotary_handler_cb(void *data, Eext_Rotary_Event_Info *ev) {
	start_screen_off_timer();

	return EINA_FALSE;
}
void register_callbacks(Evas_Object *win) {
	int result;
	result = device_add_callback(DEVICE_CALLBACK_DISPLAY_STATE, screen_state_cb,
	NULL);
	check_result(result, "device_add_callback");
	eext_rotary_event_handler_add(_rotary_handler_cb, NULL);
}
static Eina_Bool keep_on_top(void* data) {
	Evas_Object *win = data;

	elm_win_raise(win);
	elm_win_activate(win);

	LOG(DLOG_VERBOSE, "keeping the window on top, looping: %d",
			power_off_button_pressed);

	return power_off_button_pressed;
}

static void key_up_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
	Evas_Event_Key_Up *ev;

	ev = (Evas_Event_Key_Up *) event_info;
	LOG(DLOG_INFO, "Key up : %s", ev->keyname);

	if (strcmp(ev->keyname, "XF86PowerOff") == 0) {
		if (is_app_running("org.tizen.poweroff-syspopup")
				|| is_app_running("org.tizen.powerkey-syspopup")
				|| is_app_running("com.samsung.poweroff-syspopup")
				|| is_app_running("com.samsung.powerkey-syspopup")) {
			Evas_Object *win = data;
			evas_object_hide(win);
			evas_object_del(win);
			unregister_callbacks();
			turn_screen_on();
			Evas_Object* popup_win = show_disabled_popup();
			ecore_timer_add(3, hide_window, popup_win);
		}
	} else {
		start_screen_off_timer();
	}
}

static void key_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info) {
	Evas_Event_Key_Down *ev;

	ev = (Evas_Event_Key_Down *) event_info;
	LOG(DLOG_INFO, "Key up : %s", ev->keyname);

	if (strcmp(ev->keyname, "XF86PowerOff") == 0) {
		power_off_button_pressed = EINA_TRUE;
		Evas_Object *win = data;
		ecore_animator_add(keep_on_top, win);
	} else {
		start_screen_off_timer();
	}
}

void enter_theater_mode() {
	Evas_Object *win, *bg;

	win = create_top_most_window("Theater Mode Window");
	bg = create_window_background(win);

	elm_win_autodel_set(win, EINA_FALSE);
	evas_object_show(win);

	check_result(evas_object_key_grab(win, "XF86PowerOff", 0, 0, EINA_TRUE),
			"evas_object_key_grab");

	evas_object_event_callback_add(win, EVAS_CALLBACK_KEY_UP, key_up_cb, win);
	evas_object_event_callback_add(win, EVAS_CALLBACK_KEY_DOWN, key_down_cb,
			win);

	elm_win_fullscreen_set(win, EINA_TRUE);
	elm_win_modal_set(win, EINA_TRUE);
	elm_win_floating_mode_set(win, EINA_TRUE);
	elm_win_indicator_mode_set(win, ELM_WIN_INDICATOR_HIDE);
	elm_win_urgent_set(win, EINA_TRUE);
	elm_win_demand_attention_set(win, EINA_TRUE);
	elm_win_wm_desktop_layout_support_set(win, EINA_TRUE);

	//saves power by sparing us from some unnecessary render cycles
	elm_win_norender_push(win);

	register_callbacks(win);

	turn_screen_off();
}

static bool app_create(void *data) {
	LOG(DLOG_INFO, "app_create");
	return true;
}

static void app_control(app_control_h app_control, void *data) {
	/* Handle the launch request. */
	LOG(DLOG_INFO, "app_control");

	if (!isDnD()) {
		Evas_Object *win = show_enable_dnd_popup();
		ecore_timer_add(3, hide_window, win);
	} else {
		enter_theater_mode();
	}

}

static void app_pause(void *data) {
	/* Take necessary actions when application becomes invisible. */
	LOG(DLOG_INFO, "app_pause");
}

static void app_resume(void *data) {
	/* Take necessary actions when application becomes visible. */
	LOG(DLOG_INFO, "app_resume");
}

static void app_terminate(void *data) {
	/* Release all resources. */
	LOG(DLOG_INFO, "app_terminate");
}

static void ui_app_lang_changed(app_event_info_h event_info, void *user_data) {
	/*APP_EVENT_LANGUAGE_CHANGED*/
	char *locale = NULL;
	system_settings_get_value_string(SYSTEM_SETTINGS_KEY_LOCALE_LANGUAGE,
			&locale);
	elm_language_set(locale);
	free(locale);
	return;
}

static void ui_app_orient_changed(app_event_info_h event_info, void *user_data) {
	/*APP_EVENT_DEVICE_ORIENTATION_CHANGED*/
	return;
}

static void ui_app_region_changed(app_event_info_h event_info, void *user_data) {
	/*APP_EVENT_REGION_FORMAT_CHANGED*/
}

static void ui_app_low_battery(app_event_info_h event_info, void *user_data) {
	/*APP_EVENT_LOW_BATTERY*/
}

static void ui_app_low_memory(app_event_info_h event_info, void *user_data) {
	/*APP_EVENT_LOW_MEMORY*/
}

int main(int argc, char *argv[]) {
	int ret = 0;

	ui_app_lifecycle_callback_s event_callback = { 0, };
	app_event_handler_h handlers[5] = { NULL, };

	event_callback.create = app_create;
	event_callback.terminate = app_terminate;
	event_callback.pause = app_pause;
	event_callback.resume = app_resume;
	event_callback.app_control = app_control;

	ui_app_add_event_handler(&handlers[APP_EVENT_LOW_BATTERY],
			APP_EVENT_LOW_BATTERY, ui_app_low_battery, NULL);
	ui_app_add_event_handler(&handlers[APP_EVENT_LOW_MEMORY],
			APP_EVENT_LOW_MEMORY, ui_app_low_memory, NULL);
	ui_app_add_event_handler(&handlers[APP_EVENT_DEVICE_ORIENTATION_CHANGED],
			APP_EVENT_DEVICE_ORIENTATION_CHANGED, ui_app_orient_changed, NULL);
	ui_app_add_event_handler(&handlers[APP_EVENT_LANGUAGE_CHANGED],
			APP_EVENT_LANGUAGE_CHANGED, ui_app_lang_changed, NULL);
	ui_app_add_event_handler(&handlers[APP_EVENT_REGION_FORMAT_CHANGED],
			APP_EVENT_REGION_FORMAT_CHANGED, ui_app_region_changed, NULL);
	ui_app_remove_event_handler(handlers[APP_EVENT_LOW_MEMORY]);

	ret = ui_app_main(argc, argv, &event_callback, NULL);
	if (ret != APP_ERROR_NONE) {
		dlog_print(DLOG_ERROR, LOG_TAG, "app_main() is failed. err = %d", ret);
	}

	return ret;
}
