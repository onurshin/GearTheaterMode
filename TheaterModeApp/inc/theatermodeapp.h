#ifndef __theatermodeapp_H__
#define __theatermodeapp_H__

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "theatermodeapp"

#if !defined(PACKAGE)
#define PACKAGE "com.onurshin.tizen.theatermodeapp"
#endif


void enter_theater_mode();
Evas_Object *create_top_most_window();
Evas_Object *create_window_background(Evas_Object *win);
void register_callbacks();
void unregister_callbacks();

#endif /* __theatermodeapp_H__ */
