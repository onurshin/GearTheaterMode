#ifndef SIMPLE_LOG_H_
#define SIMPLE_LOG_H_

#include <dlog.h>
#include <stdbool.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "theatermode.app"
#ifdef  LOG_LEVEL
#undef  LOG_LEVEL
#endif
#define LOG_LEVEL DLOG_DEBUG

#ifdef  LOG
#undef  LOG
#endif
#define LOG(level, ...) { if(level >= LOG_LEVEL){ dlog_print(level, LOG_TAG, __VA_ARGS__); }}

static inline int check_result(int result, const char* method) {
	bool error = result < 0;
	LOG(error ? DLOG_ERROR : DLOG_VERBOSE, "%s [%s] return value result = %d",
			method, error ? "ERROR" : "SUCCESS", result);
	return result;
}

#endif /* SIMPLE_LOG_H_ */
