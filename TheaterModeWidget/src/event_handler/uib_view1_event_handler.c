/*******************************************************************************
 * This file was generated by UI Builder.
 * User should hand edit this file.
 ********************************************************************************/

#include "app_main.h"
#include "uib_views.h"
#include "uib_views_inc.h"

#include <app_manager.h>

typedef struct _uib_view1_control_context {
/* add your variables here */

} uib_view1_control_context;

void enable_theater_mode_cb(uib_view1_view_context *vc, Evas_Object *obj,
		void *event_info) {

	app_control_h app_control;
	app_control_create(&app_control);
	app_control_set_app_id(app_control, "com.onurshin.tizen.theatermode.app");
	app_control_send_launch_request(app_control, NULL, NULL);
	app_control_destroy(app_control);
}

