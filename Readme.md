# GearTheaterMode

Gear Theater Mode is a Tizen OS app for Samsung Gear S2 and S3 that brings Android like Theater Mode feature.

## Why?
In Samsung Gear S2 and S3 watches, there is no way to disable the screen which is disturbing in the theater like low light places or if you want to sleep with the watch. Thus this has been one of the most wanted feature for these watches on XDA forums (https://forum.xda-developers.com/gear-s3/how-to/petition-to-add-screen-bezel-button-lock-to-s3-t3542558) yet, it is not implemented by Samsung.

## How it works?
There is no way to disable the screen itself using any curent Tizen OS APIs unless you have [platform level certificate](https://developer.tizen.org/development/training/web-application/understanding-tizen-programming/application-signing-and-certificates), fortunately Gear S2 and S3 devices have True Black Amoled screens, meaning if the whole screen is covered with black, it won't emit any backlight. This app basicly creates a fullscreen black window and keeps it on top of every other window.

How to use the applicaton;
- Enable Do Not Disturb Mode of your device,
- Start the Theater Mode either by using application icon or the widget,
- When you want to exit theater mode,
- Give home button a short press then a long press till the device vibrates.

## Demo Video
https://drive.google.com/file/d/1XvtTbZphzKTJ6aOpv_fHEUO5VzxlJFiYdQ/preview

## XDA Thread
https://forum.xda-developers.com/gear-s3/themes/app-geartheatermode-android-theater-t3591836

## How to build and install to your device
- Download latest version from releases tab,
- Open [Tizen Studio](https://developer.tizen.org/development/tizen-studio/download), 
- Import TheaterModeApp and TheaterModeWidget projects into Tizen Studio,
- Generate a Samsung Tizen Developer Certificate if you don't have one ([click here for how to](https://goo.gl/photos/tL9gzzdVtk1cSvdf8)),
- Enable debugging on your Gear Device and connect to it using Tizen Studio,
- In Tizen Studio, right click TheaterModeApp, select "Build Signed Package",
- Right click TheaterModeApp again select Run As -> Tizen Native Application

For more info: https://developer.tizen.org/development/training/native-application/application-development-process/running-applications

## Known Limitations
- If you have Pin protection is enabled on your Gear device, while screen is locked, watchface draws itself over every other window. There is no way (that I found out) to keep Theater Mode Window on top while device is locked. If you want to use Theater Mode while sleeping you will have to disable Pin protection, since device locks itself when it detects that you are sleeping.

## Known Bugs
- While in Theater Mode, if press home button a few times very quickly, home screen might show up for a brief moment.
- Pressing home button too many times while in Theater Mode might cause app to drain battery,
- In engineering sample devices you might not be able to exit the Theater Mode,
- In any Tizen 2.x emulator this app won't work since emulator doesn't have Do Not Disturb Mode feature,
- Keeping the application window on top of others when home button pressed is depend on some internal X11 api. Meaning this application won't work in Tizen 3.0+ since they replaced X11 with Wayland.
- If your double tab home button application is set to Recent Apps, Recent Apps application draws itself over Theater Mode Window,
- Long pressing back button on SPay enable devices,
- [Let me know](https://gitlab.com/onurshin/GearTheaterMode/issues)

## Things To Test
- What happens if an alarm goes off while in Theater Mode?
- Battery usage in a long run

## Pull requests are always welcome!
 Even the cosmetic ones, C is not really my language and this needs documenting.

## Licence

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org>

